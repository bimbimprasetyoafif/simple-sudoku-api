# Sudoku API
[![pipeline status](https://gitlab.com/bimbimprasetyoafif/simple-sudoku-api/badges/master/pipeline.svg)](https://gitlab.com/bimbimprasetyoafif/simple-sudoku-api/-/commits/master)
[![coverage report](https://gitlab.com/bimbimprasetyoafif/simple-sudoku-api/badges/master/coverage.svg)](https://gitlab.com/bimbimprasetyoafif/simple-sudoku-api/-/commits/master)

### Sudoku API with backtrack algorithm

## Usage
```
go run main.go
```
It will listen at localhost port `8009`

## REST Endpoint Usage
```
localhost:8009/check
```
Sudoku payload is json with key `board` and value array object 9x9.

Additional: Use postman json collection file called `*.postman_collection.json`

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```