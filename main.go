package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/bimbimprasetyoafif/simple-sudoku-api/sudoku"
	"log"
	"net/http"
)

type Response struct {
	Message string `json:"message"`
}

func main() {
	port := "8009"

	http.HandleFunc("/check", SudokuRequestHandler())

	log.Println(fmt.Sprintf("listening on : %s", port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func SudokuRequestHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println(fmt.Sprintf("receive from %s", r.Header["User-Agent"]))

		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)
			response := Response{
				Message: "method not allowed",
			}
			_ = json.NewEncoder(w).Encode(response)
			return
		}

		var boardPayload sudoku.BoardPayload
		if err := json.NewDecoder(r.Body).Decode(&boardPayload); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			response := Response{
				Message: "your body is so cool",
			}
			_ = json.NewEncoder(w).Encode(response)
			return
		}

		if !boardPayload.SimpleBacktrack() {
			w.WriteHeader(http.StatusInternalServerError)
			response := Response{
				Message: "check your table and try again later",
			}
			_ = json.NewEncoder(w).Encode(response)
			return
		}

		if err := json.NewEncoder(w).Encode(boardPayload.Board); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			response := Response{
				Message: "try again later",
			}
			_ = json.NewEncoder(w).Encode(response)
		}
	}
}
