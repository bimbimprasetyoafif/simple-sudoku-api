package sudoku

type BoardPayload struct {
	Board [9][9]int `json:"board"`
}

func (b BoardPayload) findEmpty() (row, col int, empty bool) {
	for i := range b.Board {
		for j := range b.Board[i] {
			if b.Board[i][j] == 0 {
				return i, j, true
			}
		}
	}

	return -1, -1, false
}

func (b BoardPayload) isValid(guess, row, col int) bool {
	for i := range b.Board {
		if b.Board[row][i] == guess && col != i {
			return false
		}
	}

	for i := range b.Board {
		if b.Board[i][col] == guess && row != i {
			return false
		}
	}

	boxRow := (row / 3) * 3
	boxCol := (col / 3) * 3

	for i := boxRow; i < boxRow+3; i++ {
		for j := boxCol; j < boxCol+3; j++ {
			if b.Board[i][j] == guess && (i != row && j != col) {
				return false
			}
		}
	}

	return true
}

func (b *BoardPayload) SimpleBacktrack() bool {
	row, col, empty := b.findEmpty()
	if !empty {
		return true
	}

	for guess := 1; guess < 10; guess++ {
		if b.isValid(guess, row, col) {
			b.Board[row][col] = guess

			if b.SimpleBacktrack() {
				return true
			}

			b.Board[row][col] = 0
		}
	}

	return false
}

/*
func (b BoardPayload) PrintBoard() {
	fmt.Println("+-------+-------+-------+")
	for row := 0; row < 9; row++ {
		fmt.Print("| ")
		for col := 0; col < 9; col++ {
			if col == 3 || col == 6 {
				fmt.Print("| ")
			}
			fmt.Printf("%d ", b.Board[row][col])
			if col == 8 {
				fmt.Print("|")
			}
		}
		if row == 2 || row == 5 || row == 8 {
			fmt.Println("\n+-------+-------+-------+")
		} else {
			fmt.Println()
		}
	}
}
*/
